package com.goldenray.calculadoradegorjeta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {
    private TextView valorTot,porcent,visuPorcent;
    private SeekBar seekPorcent;
    private EditText inputValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //iniciando variáveis
        valorTot = findViewById(R.id.textTot);
        porcent = findViewById(R.id.textPorcentagem);
        seekPorcent = findViewById(R.id.seekPorcent);
        visuPorcent = findViewById(R.id.textVisu);
        inputValor = findViewById(R.id.inputVal);

        //configuirando seekbar
        seekPorcent.setMax(30);
        seekPorcent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                visuPorcent.setText(i +"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(!(TextUtils.isEmpty(inputValor.getText().toString()))) {
                    Double valor =Double.parseDouble(inputValor.getText().toString());
                    Double valorPorcent = calculaPorcentagem(seekBar.getProgress(),valor);
                    Toast.makeText(getApplicationContext(),""+ seekBar.getProgress(),Toast.LENGTH_SHORT).show();
                    porcent.setText( "R$:"+valorPorcent);
                    valorPorcent = valor +valorPorcent;
                    valorTot.setText("R$:"+valorPorcent);

                }
            }
        });
    }
    public Double calculaPorcentagem(int i,Double valor){
        Double resultado = valor*(i/100);
        return resultado;

    }

}